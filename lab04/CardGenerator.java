//Zachary Ruffin
//2-16-2018
//CSE2

//This program should give me the corresponding card to a random number between between 1 and 52. 

public class CardGenerator{                         
   	public static void main(String[] args) {
      int card;
      card = ((int)(Math.random()*52)+1);  
      String Suit="Card";
      String cardString="Card";
      
     if ( card <=13) {
             Suit= "of Diamonds";
             }
       else if ( card <= 26) {
              Suit = "of Clubs";
          }  
       else if ( card <= 39) {
              Suit = "of Hearts";
         }
       else if ( card <=52) {
            Suit = "of Spades";
     }
     
      switch (card%13) {
        case 1:
              cardString="Ace";
              break;
        case 2: 
              cardString = "2";
              break;
        case 3: 
             cardString = "3";
              break;
        case 4: 
             cardString = "4";
              break;
        case 5: 
              cardString = "5";
              break;     
        case 6: 
              cardString="6";
              break;
        case 7: 
              cardString="7";
              break;
        case 8:   
              cardString="8";
              break;
        case 9: 
             cardString="9";
              break;
        case 10: 
              cardString="10";
              break;
        case 11: 
              cardString="Jack";
              break;
        case 12: 
              cardString="Queen";
              break;
       default: 
              cardString="King";
              break;
      }

     System.out.println("You picked the "+cardString+" "+Suit+".");
    } //end of main method
} //end of class