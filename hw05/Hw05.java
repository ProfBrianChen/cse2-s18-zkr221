//Zachary Ruffin
//3-5-2018
//CSE2

//This program should print details for a class based on information provided by the user, using loops to ensure the user
//enters the correct information. 

import java.util.Scanner; 

public class Hw05 {
  public static void main(String[] args) {
  
    Scanner myScanner = new Scanner( System.in );
    
    //User inputs:
    System.out.println("Please enter the name of your department:");
    String subject = myScanner.next();  //this asks for the user to submit the name of the subject, which is placed in the variable subject
  
    
    System.out.println("What is your instructor's name?");
    String teacher = myScanner.next(); //this lets whatever you enter be assigned to the variable teacher, which represents their name
    
   
    int CourseNum=0;
    int Meets=0;
    int Students=0; //introduces int variables for the course number, number of weekly meetings, and number of students, all of which are initially set to 0
    double starttime = 0; //introduces a double variable for the class's start time, which is initially set to 0. 
    boolean CN=true;
    boolean MT=true;
    boolean ST= true; 
    boolean SB = true; //introduces boolean values for the course number, meeting times, start time, and student body number respectively
   
   System.out.println("Please enter your course subject's number:");  //Asks for the number of your course
   while (CN) {
     if (myScanner.hasNextInt()) {   //Sets variable for course number equal to whatever integer you enter 
      CourseNum = myScanner.nextInt();
       CN = false;  //has CN equal false to stop further looping, now that the answer is acceptable
     }   
     else {
       System.out.println("Error. Incorrect value. Please re-enter your information."); //Prints error upon wrong answers
       myScanner.next(); //Clears your previous answer and waits for a correct format
     }  
   }
    
   System.out.println("What time does your class start? (Please enter in form XX.XX)"); //Asks for the class starting time
     while (ST) {
     if (myScanner.hasNextDouble()) { //If given a time (in decimal form), the program will accept your answer and only run once since ST has been changed to false after running.
      starttime = myScanner.nextDouble();
       ST = false;
     }
     else {
       System.out.println("Error. Incorrect value. Please re-enter your information."); //prints error
       myScanner.next(); //clears out your response and waits for a double value
     }  
   }
    
   System.out.println("How many times a week does your class meet?"); //Asks for the number of meeting periods
   while (MT) {
     if (myScanner.hasNextInt()) { //Has the variable meets equal to your input if it is an integer, and only runs once since MT is afterwards false.
     Meets = myScanner.nextInt();  
       MT = false;  
     }
     else {
       System.out.println("Error. Incorrect value. Please re-enter your information."); //Prints error upon inacceptable input
       myScanner.next(); //Clears your response and continues to ask for new input until it meets the criteria in the above if section
     }
    }
      
   System.out.print("How many students are in your class?"); //Asks for the number of students in your class
    while (SB) {
     if (myScanner.hasNextInt()) {  //Has the students variable assigned to your response, if an integer
      Students = myScanner.nextInt();  
       SB = false; //Changes SB to a false boolean so that it doesn't continuously loop after running once
     }
     else {  
       System.out.println("Error. Incorrect value. Please re-enter your information."); //Prints error
       myScanner.next(); //Clears your response and continues to ask for new input until your answer is an integer
     }
   }
    
  //Prints out all inputs:
  System.out.println(" "+subject+", "+CourseNum+", "+teacher+", Starts at "+starttime+", "+Students+" students, Meets "+Meets+" times a week.");  
    
  } //end of main method
} //end of class