//CSE2 Welcome Class
//

public class WelcomeClass {
  
   public static void main(String[] args) {
     //Prints "------WELCOME------" to the terminal window 
     System.out.println(" ----------- ");
     System.out.println(" | WELCOME | ");
     System.out.println(" ----------- ");
     System.out.println("  ^  ^  ^  ^  ^  ^ ");
     System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\ ");
     System.out.println("<-Z--K--R--2--2--1-> ");
     System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ / ");
     System.out.println( "  v  v  v  v  v  v ");
  }
}