//Zachary Ruffin
//2-5-2018
//CSE2

//This program should be able to calculate the cost of each item, the total price, 
//and both the individual and total sales tax, in addition to the full total. 

public class Arithmetic {
    
   	public static void main(String[] args) {
    //input data:
    int numPants = 3; //the variable for the number of pants
    double pantsPrice = 34.98; //the variable for the cost of each pair of pants
    int numShirts = 2; //the variable for the number of shirts
    double shirtPrice = 24.99; //the variable for the cost of each shirt
		int numBelts = 1; //the variable for number of belts
		double beltPrice = 33.99; //the variable for the cost of a box of belts
		double paSalesTax = .06; //the variable for the rate of sales tax in Pennsylvania
      
    //output data:
    double totalCostOfPants, totalCostOfShirts, totalCostOfBelts;  
    double totalCostOfPurchases; //introduces the variables that will be used for cost
    double TaxOnPants, TaxOnShirts, TaxOnBelts; //introduces variables used for sales tax
    double totalSalesTax;
    double totalPaid; //variable for total amount pain, including tax
    
    //calculations for each variable: 
	  totalCostOfPants= numPants * pantsPrice;
    totalCostOfShirts= numShirts * shirtPrice; 
    totalCostOfBelts= numBelts * beltPrice; 
    totalCostOfPurchases=  totalCostOfBelts + totalCostOfShirts + totalCostOfPants; 
    TaxOnPants= paSalesTax * totalCostOfPants;
		  TaxOnPants= TaxOnPants*100;
		  TaxOnPants= ( (int) TaxOnPants)/100.0;	//a TA helped me with reducing the numbers to 2 decimal points.
    TaxOnShirts= paSalesTax * totalCostOfShirts;
		  TaxOnShirts= TaxOnShirts*100;  
			TaxOnShirts= ( (int) TaxOnShirts)/100.0; 
    TaxOnBelts= paSalesTax * totalCostOfBelts;
      TaxOnBelts= TaxOnBelts*100;
			TaxOnBelts= ( (int) TaxOnBelts)/100.0; 
    totalSalesTax= TaxOnBelts + TaxOnShirts + TaxOnPants; 
			totalSalesTax= totalSalesTax*100;
			totalSalesTax= ( (int) totalSalesTax)/100.0;
    totalPaid= totalCostOfPurchases + totalSalesTax; 
      
    	
   //Print out the output data.
    System.out.println("---------------");
    System.out.println("The total cost of Pants was $"+ totalCostOfPants +".");
	  System.out.println("The sales tax for the Pants was $"+ TaxOnPants +".");
    System.out.println("The total cost of Shirts was $"+ totalCostOfShirts +".");
	  System.out.println("The sales tax for the Shirts was $"+ TaxOnShirts +".");
    System.out.println("The total cost of Belts was $"+ totalCostOfBelts +".");
    System.out.println("The sales tax for the Belts was $"+ TaxOnBelts +".");
    System.out.println("The total cost of items without tax was $"+ totalCostOfPurchases +".");
    System.out.println("The total sales tax was $"+ totalSalesTax +".");
    System.out.println("The total amount paid was $"+ totalPaid +".");
   
    } //end of main
}  //end of class