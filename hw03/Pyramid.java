//Zachary Ruffin
//2-12-2018
//CSE2

//This program should calculate the volume of a pyramid when given its dimensions. 

import java.util.Scanner;

public class Pyramid{                         
   	public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    //Pyramid Dimensions:  
    System.out.print("Enter the base length of the pyramid (input length):");
    double length = myScanner.nextDouble(); //asks for the length input
    System.out.print("Enter the base width of the pyramid (input width):");
    double width = myScanner.nextDouble(); //asks for the width input
    System.out.print("Enter the height of the pyramid (input height):");
    double height = myScanner.nextDouble(); //asks for the height input
   
   
    //Volume Calculation:  
    double Volume; //introduces Volume variable
      Volume= (height*width*length); //multiplies all the dimensions
      Volume /=3; //divides dimensions by 3 or multiplies all the dimensions by a third
      Volume = Volume * 100; 
      Volume = ( (int) Volume)/100.0; //converts the volume into an integer with 2 decimal places
      System.out.println("The volume of the pyramid is "+Volume+"." ); //prints the result

    } //end of main method   
} //end of class
