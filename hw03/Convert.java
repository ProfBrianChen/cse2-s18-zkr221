//Zachary Ruffin
//2-12-2018
//CSE2

//This program should be able to calculate the amount of water hurricanes drop on areas of land, by 
//asking for the number of acres of land and how many inches of rain were dropped on average. 

import java.util.Scanner;

public class Convert{                         
   	public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    //Number of Acres and Total Rainfall:  
    System.out.print("Enter the affected area in acres (in the form xx.xx):");
    double acres = myScanner.nextDouble(); //asks for number of affected acres
    System.out.print("Enter the rainfall in the affected area, in inches (in the form xx.xx):" );
    double rainfall = myScanner.nextDouble(); //asks for the rainfall in inches for the affected acres
   
    //Cubic Miles Calculations:
    double cubicmiles; //introduces variable for cubic miles
    acres= acres * .0015625; // converts from acres to miles squared
    rainfall = rainfall * 1.578e-5;  //converts from inches to miles
    cubicmiles = acres * rainfall;   //I received help from a TA on this section.
    System.out.println( " "+cubicmiles+" cubic miles"); //prints number of cubic miles
  

    } //end of main method   
} //end of class
