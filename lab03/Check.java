// Zachary Ruffin 
//2-9-2018
//CSE2 Check: This program will evenly split up a check that needs to be paid between people, including the tip, 
//and total amount paid. With it, each person will know how much money he or she needs to pay. 

import java.util.Scanner;

public class Check{                         
   	public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    //The Tip Percent and the Check Total Desired:  
    System.out.print("Enter the original cost of the check in the form xx.xx:");
    double checkCost = myScanner.nextDouble();
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx):" );
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //converts the tip to a decimal
   
    //People who need to pay:  
    System.out.print("Enter the number of people who went out to dinner:");
    int numPeople = myScanner.nextInt();
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies; //variables for each measure of currency  
      totalCost = checkCost * (1 + tipPercent);
      costPerPerson = totalCost / numPeople; //calculations to get the cost per person
      dollars=(int)costPerPerson;
      dimes=(int)(costPerPerson * 10) % 10;  //calculations to get the remainder cents by using module function for each person
      pennies=(int)(costPerPerson * 100) % 10;
      System.out.println("Each person in the group owes $"+dollars+"."+dimes+""+pennies+"." );

    } //end of main method   
} //end of class
