//Zachary Ruffin
//2-18-2018
//CSE2

//This program should score the random roll of a dice. It should print out:
//the upper section initial total, upper section total including bonus, lower section total, and grand total. 
import java.util.Scanner;

public class Yahtzee{                         
   	public static void main(String[] args) {
        
      //Roll:
     
      Scanner myScanner = new Scanner( System.in );
      
      System.out.print("Do you want to pick your own number? (Y or N):");
      String answer = myScanner.nextLine(); //This allows for the choice of entering your own number
      int number = 0;
      int dieroll1, dieroll2, dieroll3, dieroll4, dieroll5 = 0; 
      
      if (answer.equals("Y")) {
            System.out.println("Enter a 5 digit number");
              number = myScanner.nextInt();   
              dieroll1 = (int) (number/10000)%10;
              dieroll2 = (int) (number/1000)%10;
              dieroll3 = (int) (number/100)%10; 
              dieroll4 = (int) (number/10)%10;
              dieroll5 = (int) number%10;       //If you enter your own 5 digit number, these calculations will allow you to have each var
                                                //equal the corresponding digit.
      }
       else {
             dieroll1 = (int) ((Math.random() * 6) + 1);
             dieroll2 = (int) ((Math.random() * 6) + 1);
             dieroll3 = (int) ((Math.random() * 6) + 1);
             dieroll4 = (int) ((Math.random() * 6) + 1);
             dieroll5 = (int) ((Math.random() * 6) + 1); //If you don't enter your own number, a random roll will occur for each of the 5 die. 
        }
      if (number/100000>1) {
        System.out.println("That roll has too many die.");
        System.exit(0);  //This prints a system error for any number with large digits.
      }
       else if (dieroll1 < 1 || dieroll1>6 || dieroll2 <1 || dieroll2>6 || dieroll3<1 || dieroll3>6 || dieroll4<1 || dieroll4>6 || dieroll5<1 || dieroll5>6) {
         System.out.println("Error. At least one of your rolls is wrong");
         System.exit(0);      //These or statements will print an error statement if at least one of the die has a number less than one or greater than 6.
       }
      
     System.out.println("Your dice roll is " +dieroll1+dieroll2+dieroll3+dieroll4+dieroll5+" .");
      
     //Upper Section, including bonus:
      int uppersec;  //introduces the variable for the upper section score. 
      int bonus; //introduces bonus variable
      uppersec = dieroll1 + dieroll2+ dieroll3+ dieroll4+ dieroll5; //sets the upper section variable equal to the sum of the dice numbers. 
      
      if (uppersec>=63) {
        bonus=uppersec+35;   //This will add a bonus of 35 if the sum of the dice rolls is greater than or equal to 63. 
      }   
      
      //Lower Section:
      int lowersec=0; //introduces the variable for the lower section score. 
      
      switch (lowersec) {
        case 1:    //Yahtzee
           if (dieroll1=dieroll2) {
            if (dieroll2=dieroll3){
              if (dieroll3=dieroll4){
                if (dieroll4=dieroll5){
                  lowersec = 50;
                }
              }
            }
          }
          break;
        case 2:     //Large Straight
          if (dieroll1<dieroll2) {
            if (dieroll2<dieroll3){
              if (dieroll3<dieroll4){
                if (dieroll4<dieroll5){
                  lowersec = 40;
                }
              }
            }
          }
           break;
        case 3:   //Small Straight
          if (dieroll1<dieroll2 || dieroll2<dieroll3) {
            if (dieroll2<dieroll3 || dieroll3<dieroll4){
              if (dieroll3<dieroll4 || dieroll4<dieroll5){
                  lowersec = 30;
                }
              }
            }
           break;
        case 4:   //Chance
          lowersec = dieroll1 + dieroll2+ dieroll3+ dieroll4+ dieroll5; 
          break;
        case 5: // Full House [Can't figure out why it won't compile]
          if ( dieroll1=dieroll2 || dieroll1=dieroll3 || dieroll1=dieroll4 || dieroll1=dieroll5 || dieroll2=dieroll3 || 
              dieroll2=dieroll4 || dieroll2=dieroll5 || dieroll3=dieroll4 || dieroll1=dieroll5 || dieroll4=dieroll5 ) {
            if (dieroll3=dieroll4 && dieroll4=dieroll5 || dieroll2=dieroll4 && dieroll4=dieroll5 || dieroll12=dieroll3 && dieroll3=dieroll5 || 
                dieroll2=dieroll3 && dieroll3=dieroll4 || dieroll1=dieroll4 && dieroll4=dieroll5 || dieroll1=dieroll3 && dieroll3=dieroll5 ||
               dieroll1=dieroll3 && dieroll3=dieroll4 || dieroll1=dieroll2 && dieroll2=dieroll5 ||dieroll2=dieroll3 && dieroll3=dieroll4|| dieroll1=dieroll2 && dieroll2=dieroll3){
                  lowersec = 30;
                }
            }
          break;
        case 6: //Four of a Kind [Can't figure out a different way to do this that works]
          if ( dieroll1=dieroll2 || dieroll1=dieroll3 || dieroll1=dieroll4 || dieroll1=dieroll5 || dieroll2=dieroll3 || 
              dieroll2=dieroll4 || dieroll2=dieroll5 || dieroll3=dieroll4 || dieroll1=dieroll5 || dieroll4=dieroll5 ) {
            if (dieroll3=dieroll4 && dieroll4=dieroll5 || dieroll2=dieroll4 && dieroll4=dieroll5 || dieroll12=dieroll3 && dieroll3=dieroll5 || 
                dieroll2=dieroll3 && dieroll3=dieroll4 || dieroll1=dieroll4 && dieroll4=dieroll5 || dieroll1=dieroll3 && dieroll3=dieroll5 ||
               dieroll1=dieroll3 && dieroll3=dieroll4 || dieroll1=dieroll2 && dieroll2=dieroll5 ||dieroll2=dieroll3 && dieroll3=dieroll4|| dieroll1=dieroll2 && dieroll2=dieroll3){
                  lowersec = 30;
                }
            }
          lowersec = dieroll1 + dieroll2+ dieroll3+ dieroll4+ dieroll5;
          break;
        case 7: //Three of a Kind [Can't figure out why this won't compile]
          if (dieroll1+dieroll2+dieroll3 = 3*dieroll1 || dieroll2+dieroll3+dieroll4 = 3*dieroll2 ||
             dieroll3+dieroll4+dieroll5 = 3*dieroll3|| dieroll4+dieroll5+dieroll1= 3*dieroll4 || dieroll1+dieroll4+dieroll3 = 3*dieroll1 ||
             dieroll1 + dieroll2 + dieroll5 = 3*dieroll1 || dieroll1 + dieroll3 + dieroll5 = 3*dieroll1 ){
            lowersec = dieroll1 + dieroll2+ dieroll3;
          }
          break;
          
      }
    int grandtotal; //variable for the full sum
    grandtotal = lowersec + uppersec + bonus;

System.out.println("The upper section total is " +uppersec+" .");   //prints out the upper section total
System.out.println("The upper section total, with bonus, is" +bonus+" ."); //prints out the upper section total, inclusding the bonus
System.out.println("The lower section total is" +lowersec+" ."); //prints out the lower section total
System.out.println("The grand total, is" +grandtotal+" .");     //prints out the total points earned

     
    } // end of main method

} // end of public class