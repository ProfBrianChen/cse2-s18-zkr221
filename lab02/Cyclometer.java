//Zachary Ruffin
//2-1-2018
//CSE2 Cyclometer

//This program should be able to print out the time it takes for bikes to get to certain miles 
//and the amount of rotations the front wheel goes through. 

public class Cyclometer {
    
   	public static void main(String[] args) {
    //input data:
    int secsTrip1 = 480; //the variable for the amount of time of the first trip
    int secsTrip2 = 3220; //the variable for the amount of time of the second trip
    int countsTrip1 = 1561; //the variable for how many rotations make up the first trip
    int countsTrip2 = 9037; //the variable for how many rotations make up the secnod trip
      
    //intermediate variables and output data: 
    double wheelDiameter = 27.0; //variable for the diameter of the front wheel
    double PI = 3.14159; //the value of Pi 
    int feetPerMile=5280; //how many feet equal one mile
    int inchesPerFoot=12; //how many inches are in one foot
    int secondsPerMinute=60;//how many seconds are in one minute
    double distanceTrip1, distanceTrip2, totalDistance; //introduces the variables that will be used for distance
      
    System.out.println("Trip 1 took "+
       	  (secsTrip1/secondsPerMinute)+" minutes and had "+
       	      countsTrip1+" counts.");
	       System.out.println("Trip 2 took "+
       	     (secsTrip2/secondsPerMinute)+" minutes and had "+
       	      countsTrip2+" counts.");
    //runs the calculations; stores the values of the variables. Documents your
		//calculation for how many minutes and rotations each trip took
		//
	
	  distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance for Trip 1 miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile; //Gives distance for Trip 2 in miles
	  totalDistance=distanceTrip1+distanceTrip2; //Adds the two distances together for the sum total 
	
      
   //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
	  System.out.println("Trip 2 was "+distanceTrip2+" miles");
	  System.out.println("The total distance was "+totalDistance+" miles");

	}  //end of main method   
} //end of class

